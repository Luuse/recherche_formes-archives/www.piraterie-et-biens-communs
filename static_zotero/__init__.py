from pyzotero import zotero
from jinja2 import Environment, FileSystemLoader
from distutils.dir_util import copy_tree
import os
import pyinotify
import asyncio


class StaticZotero:

    def __init__(self):
        self.token = ''
        self.id = ''
        self.type = ''
        self.templatesDir = 'templates/'
        self.defaultTemplateFile = 'main.html'
        self.siteDir = 'site/'
        self.statics = 'statics'
        self.items = {}
        self.collections = {}
        self.port = 8888
        self.tags = []

    def loadItems(self):
        try:
            if not os.path.exists(self.siteDir):
                os.mkdir(self.siteDir)

            zot = zotero.Zotero(self.id, self.type, self.token)
            self.items = zot.top()
            self.collections = zot.collections()
            documents = []
            for item in self.items:
                attachments = zot.children(item['key'])
                item.update({'attachments': attachments})
                for tag in item['data']['tags']:
                    self.tags.append(tag['tag'])
                documents.append(item)


            print(documents)

            self.items = documents
            self.tags = list(dict.fromkeys(self.tags))


        except:
            print('error API ZOTERO')

    def templateToSite(self):
        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)
        template = env.get_template('main.html')
        output = template.render(documents=self.items, collections=self.collections, tags=self.tags)


        with open(self.siteDir+'index.html', "w") as fh:
            fh.write(output)

        copy_tree('templates/statics/', 'site/')

    def build(self):
        self.loadItems()
        self.templateToSite()

    def watch(self):
        self.build()
        self.templateToSite()
    
        def handle_read_callback(event):
            self.templateToSite()
            print('------->    ', event.process_events)
            print(dir(event))

        wm = pyinotify.WatchManager()
        loop = asyncio.get_event_loop()
        notifier = pyinotify.AsyncioNotifier(wm, loop,
                                             callback=handle_read_callback)
        wm.add_watch('templates/', pyinotify.IN_MODIFY)
        loop.run_forever()
        notifier.stop()


            
    class Serve:
        def run(self):
            # self.build()
            # self.templateToSite()
            from http.server import HTTPServer, CGIHTTPRequestHandler
            import threading

            def start_server(path, port=8000):
                '''Start a simple webserver serving path on port'''
                os.chdir(path)
                httpd = HTTPServer(('', port), CGIHTTPRequestHandler)
                httpd.serve_forever()

            port = 8000
            daemon = threading.Thread(name='daemon_server',
            target=start_server,
            args=('site/', port))
            daemon.setDaemon(False) # Set as a daemon so it will be killed once the main thread is dead.
            daemon.start()

#
# print(documents)
#
# file_loader = FileSystemLoader('templates')
# env = Environment(loader=file_loader)
# template = env.get_template('main.html')
# output = template.render(documents=items)
#
# with open("site/index.html", "w") as fh:
#     fh.write(output)
#
# copy_tree("templates/css", "site/css")
#
